# -*- coding: utf-8 -*-
# Makes you be gone :D

import weechat

SCRIPT_NAME = "gone"
SCRIPT_AUTHOR = "Roliga"
SCRIPT_VERSION = "0.1"
SCRIPT_LICENSE = "GPL3"
SCRIPT_DESC = "Gone command"

def find_buffer():
    """
    Find the buffer when the plugin starts
    """
    infolist = weechat.infolist_get("irc_server", "", "")
    while weechat.infolist_next(infolist):
        topic = weechat.infolist_string(infolist, "title")
        name = weechat.infolist_string(infolist, "name")
        weechat.prnt("", weechat.infolist_fields(infolist))
        weechat.prnt("", name)
        weechat.prnt("", weechat.config_get_plugin(name))
        weechat.config_set_plugin("weechat", "derp")
        break
    weechat.infolist_free(infolist)

def command_cb(data, buffer, args):
    if args == "":
        message = "away"
    else:
        message = args

    infolist = weechat.infolist_get("irc_server", "", "")

    while weechat.infolist_next(infolist):
        name = weechat.infolist_string(infolist, "name")
        server_buffer = weechat.infolist_pointer(infolist, "buffer")

        if not server_buffer:
            continue

        if weechat.config_is_set_plugin(name):
            weechat.command(server_buffer, "/away " + weechat.config_get_plugin(name))
        else:
            weechat.command(server_buffer, "/away " + message)

    weechat.infolist_free(infolist)

    return weechat.WEECHAT_RC_OK

def main():
    weechat.hook_command("gone", "Makes you go away!",
        "[message]",
        "   message: optional away message for servers that don't have this message set in plugins.var.python.gone.[server name]. This defaults to 'away' if not set.",
        "away"
        " zzz",
        "command_cb", "")

if __name__ == "__main__":
    if weechat.register(SCRIPT_NAME, SCRIPT_AUTHOR, SCRIPT_VERSION,
                        SCRIPT_LICENSE, SCRIPT_DESC, "", ""):
        main()
